import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Cuadrado extends Application {

    private double clickInitialX;
    private double clickInitialY;
    private double clickFinalX;
    private double clickFinalY;
    private boolean band = true;
    private double xincrement = 0;
    private double yincrement = 0;
    private Circle circle;
    private int length = 0;
    private Group root;
    private int aux;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new Group();
        Scene scene = new Scene(root, 640, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Cuadrados");
        primaryStage.show();

        scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (band) {
                    clickInitialX = click.getX();
                    clickInitialY = click.getY();
                    System.out.println("ClickI X:" + clickInitialX + " ClickI Y:" + clickInitialY);
                    band = false;
                } else {
                    clickFinalX = click.getX();
                    clickFinalY = click.getY();
                    System.out.println("ClickF X:" + clickFinalX + " ClickF Y:" + clickFinalY);
                    graphicsSquare(click);
                    band = true; // termino el cuadrado, obtener nueva posición
                }
            }
        });
    }

    private void graphicsSquare(MouseEvent click) {
        // UP X
        length = (int) Math.abs(clickFinalX - clickInitialX);
        aux = (int) Math.abs(clickFinalY - clickInitialY);
        if (aux > length) {
            length = aux;
        }
        double auxX = clickInitialX;
        xincrement = ((clickFinalX - clickInitialX) / length);
        auxX++;
        for (int i = 1; i <= length; ++i) {
            circle = new Circle();
            auxX = auxX + xincrement;
            circle.setCenterX(auxX);
            circle.setCenterY(clickInitialY);
            circle.setRadius(2.0f);
            root.getChildren().addAll(circle);
        }
        // RIGHT Y
        length = (int) Math.abs(clickFinalX - clickInitialX);
        aux = (int) Math.abs(clickFinalY - clickInitialY);
        if (aux > length) {
            length = aux;
        }
        double auxY = clickInitialY;
        yincrement = ((clickFinalY - clickInitialY) / length);
        auxY++;
        for (int i = 1; i <= length; ++i) {
            circle = new Circle();
            auxY = auxY + yincrement;
            circle.setCenterX(clickInitialX);
            circle.setCenterY(auxY);
            circle.setRadius(2.0f);
            root.getChildren().addAll(circle);
        }
        // DOWN Y
        if (clickInitialY <= clickFinalY) {
            for (int i = (int) clickInitialY; i <= clickFinalY; i++) {
                circle = new Circle();
                circle.setCenterX(clickFinalX);
                circle.setCenterY(i);
                circle.setRadius(2.0f);
                root.getChildren().addAll(circle);
            }
        } else {
            for (int i = (int) clickInitialY; i >= clickFinalY; i--) {
                circle = new Circle();
                circle.setCenterX(clickFinalX);
                circle.setCenterY(i);
                circle.setRadius(2.0f);
                root.getChildren().addAll(circle);
            }
        }
        // DOWN X
        if (clickInitialX <= clickFinalX) {
            for (int i = (int) clickInitialX; i <= clickFinalX; i++) {
                circle = new Circle();
                circle.setCenterX(i);
                circle.setCenterY(clickFinalY);
                circle.setRadius(2.0f);
                root.getChildren().addAll(circle);
            }
        } else {
            for (int i = (int) clickInitialX; i >= clickFinalX; i--) {
                circle = new Circle();
                circle.setCenterX(i);
                circle.setCenterY(clickFinalY);
                circle.setRadius(2.0f);
                root.getChildren().addAll(circle);
            }
        }
    }
}