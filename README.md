﻿
# Projects of Multimedia and Desing Digital UP2020 👨🏼‍💻
This repository will contain all the projects done during the subject.

## Projects 📚

- [Adobe Pen-Tool](https://bitbucket.org/salimvzqz/proyecto181086mayoagosto2020/src/master/PenTool) ✏
	- This project is about the Adobe Pen Tool, paint lines through clicks on the scene.
	
	![Example](https://designmodo.com/wp-content/uploads/2013/01/03.jpg)
	
- [Rombo](https://bitbucket.org/salimvzqz/proyecto181086mayoagosto2020/src/master/Rombo) 🔷
	- This project prints a rhombus with asteristics, through the console.
	
	![Example](https://lh3.googleusercontent.com/u1o4sBmnuUDf3JLH3cShmZhCb7gn3aoeeb-3f6btv9xOyDgFwxszWpFkCM633YMz6QFSFSGwN7LQr9dYqY9a6ftwA465gyNOCGzmOWUT_H_xfVnS2SjpSbYEzbdsSiWTQtILfl4HB-CYi3EjP-yHy4IISFYVbdvl_POMVAURk_raQAHEjs8cctkJNVL-OHnFQdtlFEVGHAKn3CU1iGePsrFbpQ8eiM-V7jyQqm5ORxw17KpOeAgxEhJvr0r82HxlIa1Ij6AJC45m6jyRQcQLOsmryA5PnihTqtsh0mQBfMgsuP-XBnwsTfzWFgZIx-FLey3uls9aygufUPrY5k2FVFllGSsMIwp5O-Gqi-i7Vsw5ywEBQMLfSQ4nw22tvyC9ZUKfN5oZuBLU_E8sDN1N7BozX5UmAboeZav3vZ3gdtS4eL5t14YB8VaIt_b7or76l2GN-5YVHt5PhOxdNPkY79syKKP_IYg0zUqz8e2bLFQCnYp7RFek-9Y_4HeazlpgY_Km-0-TB2ygHGEnibkgaHhP_1oCEEst57ofW0hTAJ48rw-qkXJK2qCK33pQFlt7yOYkmyTIe_ZF7TzOweOKhb6aUw-0jPdRbblI5HkEUbeVR-LRvdI6If6PTB93KE_nQbeUhC4DLcv3M-6NZ_A_ctfPXZYVLg4qIMJtYVNTEiIFXOWTE1Cm5FeR-oI=w215-h280-no?authuser=0)
	
- [Rectangulo](https://bitbucket.org/salimvzqz/proyecto181086mayoagosto2020/src/master/Cuadrado) 🟥
	- This project paint rectangles through clicks on the scene.
	
	![Example](https://1.bp.blogspot.com/_wFWpuKkopyU/TBAJnG2WEHI/AAAAAAAAAHk/DEz2VUVqGvg/s320/dib_cuadrado.jpg)
	
- [Project Final](https://bitbucket.org/salimvzqz/proyecto181086mayoagosto2020/src/master/Project) 📚
	- This project is the join of the project "Pen-Tool" and "Rectangulo".
	
## Requeriments 🛠️
You need:

- [JavaFX](https://gluonhq.com/products/javafx/)

## Installation ⚙️
``
git clone https://salimvzqz@bitbucket.org/salimvzqz/proyecto181086mayoagosto2020.git
``

## Compilar y Ejecutar 🔧🚀
` javac --module-path %VARIABLE_DE_ENTORNO_JAVAFX% --add-modules javafx.controls,javafx.fxml *.java `

` java --module-path %VARIABLE_DE_ENTORNO_JAVAFX% --add-modules javafx.controls,javafx.fxml nameClass `

## Author
- [Salim Vazquez Solis](https://github.com/SalimVazquez)  👾
