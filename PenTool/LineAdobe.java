import java.util.Stack;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class LineAdobe extends Application {

    int contador = 0;
    int length = 0;
    double clickInitialX = 0;
    double clickInitialY = 0;
    double xincrement = 0;
    double yincrement = 0;
    Circle circle;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        Scene scene = new Scene(root, 640, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        Stack<Double> stackX = new Stack<>();
        Stack<Double> stackY = new Stack<>();

        scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent click) {
                circle = new Circle();
                stackX.push(click.getX());
                stackY.push(click.getY());
                if(stackX.size()==2){
                    double x2 = (double) stackX.peek();
                    double y2 = (double) stackY.peek();
                    stackX.pop();
                    stackY.pop();
                    double x = (double) stackX.peek();
                    double y = (double) stackY.peek();
                    stackX.pop();
                    stackY.pop();
                    stackX.push(x2);
                    stackY.push(y2);
                    length = (int)Math.abs(x2 - x);
                    if (Math.abs (y2 - y) > length){
                        length = (int) Math.abs (y2 - y);
                    }
                    xincrement = (double) (x2 - x) / (double) length;
                    yincrement = (double) (y2 - y) / (double) length;
                    x = x + 1;
                    y = y + 1;
                    for(int i = 1; i <= length; ++i){
                        circle = new Circle();
                        x = x + xincrement;
                        y = y + yincrement;
                        circle.setCenterX(x);
                        circle.setCenterY(y);
                        circle.setRadius(2.0f);
                        root.getChildren().addAll(circle);
                    }
                }
			}
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}