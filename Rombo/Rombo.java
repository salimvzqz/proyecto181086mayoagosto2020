import java.util.Scanner;

/**
 * Rombo
 */
public class Rombo {

    public static void main(String[] args) {
        int input;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Ingrese el numero de entrada: ");
            input = sc.nextInt();
            if (input % 2 == 0) {
                System.out.println("Se ingreso un numero par, es necesario un numero impar, intente de nuevo");
            }
        } while (input % 2 == 0);
        sc.close();
        if (input == 1) {
            System.out.println("<==Rombo Relleno==>");
            System.out.println("*");
            System.out.println("<==Rombo Hueco==>");
            System.out.println("*");
        } else {
            System.out.println("<==Rombo Relleno==>");
            // parte superior del rombo
            for (int i = 1; i <= input; i = i + 2) {
                // Añadimos los espacios necesarios delante de cada linea
                for (int k = input + 1; k >= i; k = k - 2) {
                    System.out.print(" ");
                }
                // Mostramos los asteriscos
                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
            // parte inferior del rombo
            for (int i = input; i >= 1; i = i - 2) {
                // Añadimos los espacios necesarios delante de cada linea
                for (int k = i; k <= input + 2; k = k + 2) {
                    System.out.print(" ");
                }
                // Mostramos los asteriscos
                for (int j = i - 2; j > 0; j--) {
                    System.out.print("*");
                }
                System.out.println();
            }
            System.out.println("<==Rombo Hueco==>");
            int rows = input / 2 + 1;

            for (int i = 0; i < rows; i++) {
                System.out.println();
                for (int j = 0; j < rows - i - 1; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k < (2 * i) + 1; k++) {
                    if (k == 0 || k == 2 * i || i == 0) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            for (int i = rows - 2; i >= 0; i--) {
                System.out.println();
                for (int j = 0; j < rows - i - 1; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k < i * 2 + 1; k++) {
                    if (k == 0 || i == 0 || k == 2 * i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
        }
    }
}