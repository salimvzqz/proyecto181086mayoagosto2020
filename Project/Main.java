import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import java.util.Stack;
import javafx.scene.shape.Circle;

public class Main extends Application {

    @FXML // fx:id="anchorScene"
    private AnchorPane anchorScene; // Value injected by FXMLLoader

    @FXML // fx:id="anchorPaneMenu"
    private AnchorPane anchorPaneMenu; // Value injected by FXMLLoader

    @FXML // fx:id="textMenu"
    private Label textMenu; // Value injected by FXMLLoader

    @FXML // fx:id="anchorLine"
    private AnchorPane anchorLine; // Value injected by FXMLLoader

    @FXML // fx:id="anchorRectangule"
    private AnchorPane anchorRectangule; // Value injected by FXMLLoader

    private Parent root;
    private boolean statusMenu;
    private Circle circle;
    private double x1 = 0, y1 = 0;
    private int saltos = 0, saltosX, saltosY;
    private double rutaX = 0;
    private double rutaY = 0;
    private Stack<Double> lstX = new Stack<>();
    private Stack<Double> lstY = new Stack<>();
    private double clickInitialX;
    private double clickInitialY;
    private double clickFinalX;
    private double clickFinalY;
    private boolean band = true;
    private double xincrement = 0;
    private double yincrement = 0;
    private int length = 0;
    private int aux;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = FXMLLoader.load(getClass().getResource("./Views/project.fxml"));
        primaryStage.getIcons().add(new Image("./Assets/logoUP.png"));
        primaryStage.setTitle("Project Multimedia and Design Digital");
        primaryStage.setScene(new Scene(root, 640, 400));
        primaryStage.show();
        showInformationExecution();
    }

    void showInformationExecution() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Informacion de ejecucion");
        alert.setHeaderText(null);
        // Get the Stage.
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        // Add a custom icon.
        stage.getIcons().add(new Image(this.getClass().getResource("./Assets/logoUP.png").toString()));
        String infoExecution = "Despliega el menu de la izquierda y selecciona tus figuras con el click derecho o secundario\npara dibujar, selecciona la figura y da clicks izquierdos o principales sobre el fondo blanco.";
        alert.setContentText(infoExecution);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    void clickedLine() {
        System.out.println("Start draw line with method clickedLine");
        anchorScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getButton() == MouseButton.PRIMARY) {
                    circle = new Circle();
                    // algoritmo de Berseham
                    lstX.push(click.getX());
                    lstY.push(click.getY());
                    if (lstX.size() == 2) {
                        System.out.println(lstX);
                        double x2 = (double) lstX.pop();
                        double y2 = (double) lstY.pop();

                        x1 = (double) lstX.pop();
                        y1 = (double) lstY.pop();

                        lstX.push(x2);
                        lstY.push(y2);

                        saltosX = (int) Math.abs(x2 - x1);
                        saltosY = (int) Math.abs(y2 - y1);
                        if (saltosX > saltosY) {
                            saltos = saltosX;
                        } else {
                            saltos = saltosY;
                        }
                        rutaX = (x2 - x1) / (double) saltos;
                        rutaY = (y2 - y1) / (double) saltos;
                        x1 += 1;
                        y1 += 1;
                        for (int i = 1; i <= saltos; ++i) {
                            circle = new Circle();
                            x1 += rutaX;
                            y1 += rutaY;
                            circle.setCenterX(x1);
                            circle.setCenterY(y1);
                            circle.setRadius(2.0f);
                            anchorScene.getChildren().addAll(circle);
                        }
                    }
                } else {
                    System.out.println("Stop draw line");
                    lstX.clear();
                    lstY.clear();
                }
            }
        });
    }

    @FXML
    void clickedRectangule(MouseEvent click) {
        System.out.println(
                "Start draw rectangule with method clickedRectangule in X:" + click.getX() + " Y:" + click.getY());
        anchorScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getButton() == MouseButton.PRIMARY) {
                    if (band) {
                        clickInitialX = click.getX();
                        clickInitialY = click.getY();
                        System.out.println("ClickI X:" + clickInitialX + " ClickI Y:" + clickInitialY);
                        band = false;
                    } else {
                        clickFinalX = click.getX();
                        clickFinalY = click.getY();
                        System.out.println("ClickF X:" + clickFinalX + " ClickF Y:" + clickFinalY);
                        graphicsSquare(click);
                        band = true; // termino el cuadrado, obtener nueva posición
                    }
                } else {
                    System.out.println("Stop draw rectangule");
                }
            }
        });
    }

    @FXML
    void showMenu(MouseEvent click) {
        if (statusMenu) {
            anchorPaneMenu.setLayoutX(-47.0);
            statusMenu = false;
            textMenu.setText(">");
        } else {
            anchorPaneMenu.setLayoutX(0.0);
            statusMenu = true;
            textMenu.setText("<");
        }
    }

    private void graphicsSquare(MouseEvent click) {
        // UP X
        length = (int) Math.abs(clickFinalX - clickInitialX);
        aux = (int) Math.abs(clickFinalY - clickInitialY);
        if (aux > length) {
            length = aux;
        }
        double auxX = clickInitialX;
        xincrement = ((clickFinalX - clickInitialX) / length);
        auxX++;
        for (int i = 1; i <= length; ++i) {
            circle = new Circle();
            auxX = auxX + xincrement;
            circle.setCenterX(auxX);
            circle.setCenterY(clickInitialY);
            circle.setRadius(2.0f);
            anchorScene.getChildren().addAll(circle);
        }
        // RIGHT Y
        length = (int) Math.abs(clickFinalX - clickInitialX);
        aux = (int) Math.abs(clickFinalY - clickInitialY);
        if (aux > length) {
            length = aux;
        }
        double auxY = clickInitialY;
        yincrement = ((clickFinalY - clickInitialY) / length);
        auxY++;
        for (int i = 1; i <= length; ++i) {
            circle = new Circle();
            auxY = auxY + yincrement;
            circle.setCenterX(clickInitialX);
            circle.setCenterY(auxY);
            circle.setRadius(2.0f);
            anchorScene.getChildren().addAll(circle);
        }
        // DOWN Y
        if (clickInitialY <= clickFinalY) {
            for (int i = (int) clickInitialY; i <= clickFinalY; i++) {
                circle = new Circle();
                circle.setCenterX(clickFinalX);
                circle.setCenterY(i);
                circle.setRadius(2.0f);
                anchorScene.getChildren().addAll(circle);
            }
        } else {
            for (int i = (int) clickInitialY; i >= clickFinalY; i--) {
                circle = new Circle();
                circle.setCenterX(clickFinalX);
                circle.setCenterY(i);
                circle.setRadius(2.0f);
                anchorScene.getChildren().addAll(circle);
            }
        }
        // DOWN X
        if (clickInitialX <= clickFinalX) {
            for (int i = (int) clickInitialX; i <= clickFinalX; i++) {
                circle = new Circle();
                circle.setCenterX(i);
                circle.setCenterY(clickFinalY);
                circle.setRadius(2.0f);
                anchorScene.getChildren().addAll(circle);
            }
        } else {
            for (int i = (int) clickInitialX; i >= clickFinalX; i--) {
                circle = new Circle();
                circle.setCenterX(i);
                circle.setCenterY(clickFinalY);
                circle.setRadius(2.0f);
                anchorScene.getChildren().addAll(circle);
            }
        }
    }
}
